<?php

$_DB = array(
    'log' => array (
        'host' => '%host%',
        'user' => '%user%',
        'pass' => '%pass%',
        'db' => '%db%',
        'charset' => 'utf8'
    )
);
function _startdb($db)
{
    global $_SQL,
           $_DB;

    if(isset($_SQL[$db]) && $_SQL[$db]) {
        return true;
    }
    $_SQL[$db] = mysqli_connect($_DB[$db]['host'], $_DB[$db]['user'], $_DB[$db]['pass'], $_DB[$db]['db']);
    if(!$_SQL[$db]) {
        return false;
    }
    if(isset($_DB[$db]['charset'])) {
        mysqli_set_charset($_SQL[$db], $_DB[$db]['charset']);
    }

    return true;
}
