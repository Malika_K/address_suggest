<?php

require_once 'autoload.php';

if(isset($_POST['query']) && $_POST['query']) {
    $query = (string)$_POST['query'];

    $api = new \Yandex\Geo\Api();
    // Ключ API
    $api->setToken('%key%');
    // икать по адресу
    $api->setQuery($query);
    // фильтры для поиска (кол-во подсказок, язык)
    $api
        ->setLimit(6)
        ->setLang(\Yandex\Geo\Api::LANG_RU)
        ->load();
    $response = $api->getResponse();
    // найденные локации
    $collection = $response->getList();

    $html = '';

    foreach ($collection as $index => $item) {
        // информация о найденных локациях
        $data = $item->getData();
        // детали каждой локации для дальнейшего логирования в случае, если что-то выбрано пользователем

        $details = [];

        foreach($data as $key => $value) {
            $details[] = 'data-' . strtolower($key) . '="' . $value . '"';
        }
        $html .= $details
            ? '<li class="list-group-item" ' . implode(' ', $details) . '>' . $data['Address'] . '</li>'
            : '';
    }
    print $html;
}

//EOF
