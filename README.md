  База данных
  -----------
  MariaDB

``` CREATE TABLE IF NOT EXISTS `address` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `address` varchar(900) NOT NULL,
  `kind` varchar(200) NOT NULL,
  `countrynamecode` varchar(10) NOT NULL,
  `countryname` varchar(500) NOT NULL,
  `administrativeareaname` varchar(500) NOT NULL,
  `localityname` varchar(500) NOT NULL,
  `coordinate` point NOT NULL,
  `ip` varchar(200) NOT NULL,
  `geo` varchar(200) NOT NULL,
  `ident` varchar(500) NOT NULL,
  `date` datetime NOT NULL,
  `bot` smallint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8; ```

Для поиска по адресам использован API Яндекс.Геокодирование
https://github.com/yandex-php/php-yandex-geo
