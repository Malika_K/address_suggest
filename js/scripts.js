$(document).ready(function() {

    // елемент input для ввода адреса
    var inputEl = $('#query')
    // елемент для вывода подсказок
    var suggestions =  $('#suggestions')

    // отслеживаем клики в поле ввода адреса
    inputEl.keyup(function (e) {

        var query = this.value

        // начинаем подбирать подсказки если длина слова > 3 символов
        if (query.length >= 3 ) {
            $.ajax({
                type: 'post',
                url: 'suggestions.php',
                data: { query: query },
                success: function (data) {
                    // вывод блока с подсказками
                    suggestions.html('<ul class="list-group">' + data + '</ul>')
                    // при выборе подсказки:
                    suggestions.find('li').on('click', function (event) {
                        // весь текст подставляется в форму
                        $(inputEl).val($(event.target).text())
                        // очищается блок с подсказками
                        suggestions.html('')
                        // вызывается функция логирования результата поиска
                        address_log(this)
                    })
                }
            });
        } else {
            suggestions.html('')
        }
    })
    // очищается блок с подсказками в случае клика на странице
    document.addEventListener('click', function () {
        suggestions.html('')
    }, false)
})

function getXMLHttpRequest() {
    return window.XMLHttpRequest ? new XMLHttpRequest() : null;
}

// логируем результат поиска адреса
function address_log(el) {
    var address = el.getAttribute("data-address")
    var kind = el.getAttribute("data-kind")
    var countrynamecode = el.getAttribute("data-countrynamecode")
    var countryname = el.getAttribute("data-countryname")
    var administrativeareaname = el.getAttribute("data-administrativeareaname")
    var localityname = el.getAttribute("data-localityname")
    var longitude = el.getAttribute("data-longitude")
    var latitude = el.getAttribute("data-latitude")
    if(address === null) {
        return true;
    }
    var xmlhttp = getXMLHttpRequest();
    if(xmlhttp !== null) {
        xmlhttp.onreadystatechange = function() {
            if(xmlhttp.readyState === XMLHttpRequest.DONE ) {

            }
        }
        xmlhttp.open('post', 'chosen_address.php', true)
        xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=UTF-8')
        xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest')
        xmlhttp.send(
            'address=' + address +
            '&kind=' + kind +
            '&countrynamecode=' + countrynamecode +
            '&countryname=' + countryname +
            '&administrativeareaname=' + administrativeareaname +
            '&localityname=' + localityname +
            '&longitude=' + longitude +
            '&latitude=' + latitude
        )
    }
}
