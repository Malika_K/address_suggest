<?php

require_once 'conf.php';
if(isset($_POST)) {
    foreach($_POST as $index => $item) {
        $log[$index] = strip_tags(trim($item));
    }
    if(isset($log['address']) && strlen($log['address']) >= 3) {

        $log['ip'] = '';

        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $log['ip'] = trim($_SERVER['HTTP_X_FORWARDED_FOR']);
        } elseif (isset($_SERVER['HTTP_X_REAL_IP'])) {
            $log['ip'] = trim($_SERVER['HTTP_X_REAL_IP']);
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $log['ip'] = trim($_SERVER['REMOTE_ADDR']);
        }
        $log['geo'] = (isset($_SERVER['HTTP_GEO']) ? $_SERVER['HTTP_GEO'] : '');
        $log['ident'] = (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '');
        $log['date'] = date('Y-m-d H:i:s');
        $log['bot'] = intval(isset($_SERVER['HTTP_USER_AGENT'])
            && preg_match(
                '/bot|crawl|slurp|spider|mediapartners|ia_archiver/i',
                strtolower($_SERVER['HTTP_USER_AGENT'])
            )
        );
        $coordinate = $log['latitude'] . ', ' . $log['longitude'];
        unset($log['longitude']);
        unset($log['latitude']);

        _startdb('log');

        $log['address'] = mysqli_real_escape_string($_SQL['log'], $log['address']);
        $sql_query = 'insert into `address` 
            (`' . implode('`,`', array_keys($log)) . '`, `coordinate`) 
            values 
            (\'' . implode('\',\'', $log) . '\', POINT(' . $coordinate . '))';
        mysqli_query($_SQL['log'], $sql_query);
    }
}

//EOF
